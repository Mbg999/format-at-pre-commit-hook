# format at pre-commit hook

<h5>Google Java style guide: https://google.github.io/styleguide/javaguide.html</h5>

<h3>save as .git/hooks/pre-commit</h3>

<h3>download the desired version from https://github.com/google/google-java-format/releases</h3>
<ul>
    <li>take care with your java version, if it is lower than the .jar, the formatter won't work!</li>
    <li>save the .jar as .git/hooks/google-java-format-1.3-all-deps.jar</li>
    <br>
    <li>you can change the Google formatter version in the script var: version</li>
    <li>you can change the Google formatter path in the script var: formatter</li>
    <br>
    <li>you can skip the pre-commit hook with the --no-verify flag at commit command</li>
</ul>

<h3>Eclipse formatter on save event</h3>
<p>Eclipse: Window -> Preferences -> Java -> Editor -> Save Action: check the 'Perform the selected actions on save' and 'Format source code', then use the 'Configure the formatter settings on the Formatter page' link for import the eclipse-java-google-style.xml formatter and select it.</p>


<p>Install pre-commit hook in your projects: https://gitlab.com/Mbg99/pre-commit-hook-install</p>
<p>Format all the java files of a project: https://gitlab.com/Mbg99/format-projects</p>

<p>me: https://github.com/Mbg999</p>
